class Calculator
{
	public static int add(int num1, int num2)
	{
		int sum = num1 + num2;
		return sum;
	}
	
	public static int subtract(int num1, int num2)
	{
		int difference = num1 - num2;
		return difference;
	}
	
	public int multiply(int num1, int num2)
	{
		int total = num1 * num2;
		return total;
	}
	
	public int divide(int num1, int num2)
	{
		int quotient = num1 / num2;
		return quotient;
	}
}
