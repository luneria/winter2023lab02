class MethodsTest
{
	public static void main(String[]args){
		//number 3
		int x = 5;
		//number 4
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		//number 5
		System.out.println(x);
		methodOneInputNoReturn(x+10);
		System.out.println(x);
		//number 6
		methodTwoInputNoReturn(2.6);
		
		//number 7
		int z = methodNoInputReturnInt();
		System.out.println(z);
		//number 8
		double calculRoot = sumSquareRoot(9, 5);
		System.out.println(calculRoot);
		//number 10
		String s1 = "java";
		String s2 = "programming";
		System.out.println(s1.length());
		System.out.println(s2.length());
		//number 11
		/*
		int adding1 = SecondClass.addOne(50);
		System.out.println(adding1);
		int adding2 = SecondClass.addTwo(50);
		System.out.println(adding2);
		*/
		SecondClass sc = new SecondClass();
		int finalResult = sc.addTwo(50);
		System.out.println(finalResult);
		*/
	}
	
	public static void methodNoInputNoReturn()
	{
		System.out.println("I’m in a method that takes no input and returns nothing");
		int x = 20;
		System.out.println(x);
	}
	
	public static void methodOneInputNoReturn(int youCanCallThisWhateverYouLike)
	{
		youCanCallThisWhateverYouLike -= 5;
		System.out.println("Inside the method one input no return");
		System.out.println(youCanCallThisWhateverYouLike);
	}
	
	public static void methodTwoInputNoReturn(int num1, double num2)
	{
		System.out.println(num1);
		System.out.println(num2);
	}
	
	public static int methodNoInputReturnInt()
	{
		int value = 5;
		return value;
	}
	
	public static double sumSquareRoot(int num1, int num2)
	{
		double result = num1 + num2;
		result = Math.sqrt(result);
		return result;
	}
}