import java.util.Scanner;
class PartThree
{
	public static void main(String[]args)
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter 2 numbers");
		int num1=scan.nextInt();
		int num2=scan.nextInt();
		
		Calculator sc = new Calculator();
		//method add
		int SumAnswer = sc.add(num1, num2);
		System.out.println("The addition equals to: " + SumAnswer);
		//method subtract
		int DifferenceAnswer = sc.subtract(num1, num2);
		System.out.println("The difference queals to: " + DifferenceAnswer);
		//method of multiply
		int TotalAnswer = sc.multiply(num1, num2);
		System.out.println("The multiplication equals to: " + TotalAnswer);
		//method of divide
		int QuotientAnswer = sc.divide(num1, num2);
		System.out.println("The division equals to: " + QuotientAnswer);
	}
}
